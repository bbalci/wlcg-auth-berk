# IAM K8S basic configuration and utils

## Requirements

- openssl
- kustomize

## How to deploy a new IAM instance on our K8S cluster

1. Obtain a database schema and credentials for the new IAM instance.
2. Create a namespace for your new IAM instance. Annotate the namespace name
3. Create a new iam deployment with the script utils/create-deployment.sh, as
   follows:
   ```console
   EXPERIMENT=alice NAMESPACE=alice-auth FQDN=alice-auth.web.cern.ch VOMS_FQDN=voms-alice-auth.app.cern.ch sh utils/create-deployment.sh
   EXPERIMENT=berk NAMESPACE=wlcg-auth-berk FQDN=wlcg-auth-berk.web.cern.ch VOMS_FQDN=wlcg-auth-berk.app.cern.ch sh utils/create-deployment.sh
   ```
4. Follow the instructions in the README.md file in the deployment directory
