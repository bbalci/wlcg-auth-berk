#!/bin/bash
set -e

export X509_USER_PROXY=/tmp/x509up_u$(id -u)
export BEARER_TOKEN_FILE=/tmp/bt_u$(id -u)
export REQUESTS_CA_BUNDLE=/etc/pki/tls/cert.pem

init-credentials.sh

vomsimporter --vo ${VOMS_VO} --voms-host ${VOMS_HOST} --iam-host ${IAM_HOST} \
  --skip-duplicate-accounts-checks \
  --link-cern-sso-ldap \
  --merge-accounts True
