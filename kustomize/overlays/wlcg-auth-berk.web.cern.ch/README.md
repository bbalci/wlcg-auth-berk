# IAM deployment for wlcg-auth-berk.web.cern.ch

Check the `kustomization.yaml` for deployment configuration options.

## Check that setup work as expected

```console
kubectl apply -oyaml --dry-run=client -k .
```

## Apply configuration

```console
kubectl apply -k .
```

# VOMS importer batch job configuration

## How to trigger a one-shot import execution

```bash
sh ./utils/trigger-import.sh
```

## Summary

Authentication for the [VOMS Importer script](https://github.com/indigo-iam/voms-importer)
is coordinated by an instance of [oidc-agent](https://github.com/indigo-dc/oidc-agent).

The flow is roughly as follows:

- OIDC agent starts, and reads its own config from the oidc-agent/voms-importer file
- The VOMS importer script starts and pulls all users from IAM to be able to compare to VOMS
- The script grabs a proxy from whichever IAM User matches the `sub` of the token in OIDC agent
- This proxy is used to query VOMS
- Users are synchronised from VOMS into IAM (not the other way around)

## Setup VOMS Importer

### Your IAM Account

1. Ensure you are an admin for IAM
1. Associate a grid cert to your user account
1. Ensure this cert also has admin rights on the target VOMS instance e.g. by running
    ```
    voms-admin --vo=alice --nousercert add-ACL-entry /alice '/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vomsimport/CN=773231/CN=Robot: VOMS Importer for WLCG IAM' '/DC=ch/DC=cern/CN=CERN Grid Certification Authority' 'CONTAINER_READ,MEMBERSHIP_READ,ATTRIBUTES_READ,ACL_READ,REQUESTS_READ,PERSONAL_INFO_READ' true

    ```
1. Generate a long lived voms-proxy (voms-proxy-init is installed on lxplus)
    ```
    voms-proxy-init --valid 8760:00
    ```
1. Copy the generated proxy into your IAM User profile (button `Add managed proxy certificate`)

### OIDC Agent Config Generation

1. The config will be tied to the account of whoever is completes the device authentication flow.
1. Install oidc-agent locally (a v4.2)
1. Create a client for oidc-agent called voms-importer
  (`OIDC_AGENT_ALIAS` in file `oidc-agent.secret.env`) and use the device flow to
  authenticate (this will dynamically register a client in IAM)
    ```
    eval $(oidc-agent)
    oidc-gen voms-importer -w device
    ```  
1. When prompted specify the IAM endpoint as the issuer, e.g. `https://wlcg-auth.web.cern.ch`
1. Complete the device authentication flow
1. Choose the encryption password (`OIDC_AGENT_SECRET` in file `oidc-agent.secret.env`)
1. You should now see a client in IAM, add the `proxy:generate` scope to it
1. Go back to oidc-agent and modify the config to include this new scope
    ```
    oidc-gen voms-importer -w device -m
    Enter decryption password for account config 'voms-importer':
    Client_id [c25876dd-4d55-4b2b-86d4-8f8af035464b]:
    Client_secret [***]:
    The following scopes are supported: openid profile email address phone offline_access eduperson_scoped_affiliation eduperson_entitlement eduperson_assurance
    Scopes or 'max' (space separated) [openid profile offline_access email]: openid profile offline_access proxy:generate
    Redirect_uris (space separated) [edu.kit.data.oidc-agent:/redirect http://localhost:39746 http://localhost:8080 http://localhost:4242]:
    Generating account configuration ...
    ```
1. Grab the config from .config/oidc-agent/voms-importer and save it in file `oidc-agent/voms-importer`
1. Deploy the config as usual with `kubectl apply -k .`

### Trigger job

e.g.

```
kubectl create job --from=cronjobs/lhcb-auth-voms-importer lhcb-auth-voms-importer-one-shot
```

```
kubectl create job --from=cronjobs/alice-auth-voms-importer alice-auth-voms-importer-one-shot
```
