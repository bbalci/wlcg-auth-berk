#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

OVERLAYS_DIR=${DIR}/../kustomize/overlays
TEMPLATE_DIR=${DIR}/../kustomize/.templates

if [ -z "${EXPERIMENT}" ]; then
  echo "Please set the EXPERIMENT env variable"
  exit 1
fi

if [ -z "${NAMESPACE}" ]; then
  echo "Please set the NAMESPACE env variable (with the namespace name of the IAM deployment)!"
  exit 1
fi

if [ -z "${FQDN}" ]; then
  echo "Please set the FQDN env variable (with the fqdn of the IAM deployment)!"
  exit 1
fi

VOMS_FQDN=${VOMS_FQDN:-voms.example}

TARGET_DIR=${OVERLAYS_DIR}/${FQDN}

if [ -d ${TARGET_DIR} ]; then
  echo "Deployment directory ${TARGET_DIR} already exists"
  exit 1
fi

mkdir ${TARGET_DIR}
cp -r ${TEMPLATE_DIR}/* ${TARGET_DIR}

KEY_ID=$(echo ${FQDN}| cut -d"." -f1)

## Change the README
sed -e "s#%%FQDN%%#${FQDN}#" ${TEMPLATE_DIR}/README.md > ${TARGET_DIR}/README.md

## Edit deployment template
sed -e "s#%%FQDN%%#${FQDN}#" -e "s#%%NAMESPACE%%#${NAMESPACE}#" ${TEMPLATE_DIR}/deployment-name.patch.yaml > ${TARGET_DIR}/deployment-name.patch.yaml

## Edit custom volumes template
sed -e "s#%%FQDN%%#${FQDN}#" -e "s#%%NAMESPACE%%#${NAMESPACE}#" ${TEMPLATE_DIR}/add-custom-config-volumes.patch.yaml > ${TARGET_DIR}/add-custom-config-volumes.patch.yaml

## Edit login-service env variables
sed -e "s#%%FQDN%%#${FQDN}#" -e "s#%%KEY_ID%%#${KEY_ID}#" -e "s#%%EXPERIMENT%%#${EXPERIMENT}#" ${TEMPLATE_DIR}/login-service/login-service.env > ${TARGET_DIR}/login-service/login-service.env

## Edit the login service nginx passthrough config
sed -e "s#%%FQDN%%#${FQDN}#" -e "s#%%NAMESPACE%%#${NAMESPACE}#" ${TEMPLATE_DIR}/nginx/config/iam.conf > ${TARGET_DIR}/nginx/config/iam.conf

## Edit logging config
sed -e "s#%%EXPERIMENT%%#${EXPERIMENT}#" ${TEMPLATE_DIR}/flume/flume.conf > ${TARGET_DIR}/flume/flume.conf

## Edit the CERN specific config
sed -e "s#%%EXPERIMENT%%#${EXPERIMENT}#" ${TEMPLATE_DIR}/cern/application-cern.yml > ${TARGET_DIR}/cern/application-cern.yml

## Edit the AUP
sed -e "s#%%EXPERIMENT%%#${EXPERIMENT}#" -e "s#%%FQDN%%#${FQDN}#" ${TEMPLATE_DIR}/assets/aup.html > ${TARGET_DIR}/assets/aup.html

## Edit the VOMS Importer env
sed -e "s#%%EXPERIMENT%%#${EXPERIMENT}#" -e "s#%%FQDN%%#${FQDN}#" ${TEMPLATE_DIR}/voms-importer/voms-importer.env > ${TARGET_DIR}/voms-importer/voms-importer.env

## Edit namespace name in kustomization
sed -e "s#%%NAMESPACE%%#${NAMESPACE}#" -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/kustomization.yaml > ${TARGET_DIR}/kustomization.yaml

## Generate JSON web keys
sh ${DIR}/jwk/generate-jwks.sh > ${TARGET_DIR}/json-keystore.jwks

## LSC config
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/vomsdir/template.lsc > ${TARGET_DIR}/voms-aa/vomsdir/${VOMS_FQDN}.lsc

## Edit namespace in voms route
sed -e "s#%%NAMESPACE%%#${NAMESPACE}#" ${TEMPLATE_DIR}/voms-aa/voms-aa.route.yaml > ${TARGET_DIR}/voms-aa/voms-aa.route.yaml

## NGINX config
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/nginx/conf.d/voms-aa.conf > ${TARGET_DIR}/voms-aa/nginx/conf.d/voms-aa.conf

## Deployment name patch
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/voms-aa.patch.yaml > ${TARGET_DIR}/voms-aa/voms-aa.patch.yaml

sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" -e "s#%%EXPERIMENT%%#${EXPERIMENT}#" ${TEMPLATE_DIR}/voms-aa/voms-aa.env > ${TARGET_DIR}/voms-aa/voms-aa.env

## Deployment name patch
sed -e "s#%%FQDN%%#${FQDN}#" -e "s#%%NAMESPACE%%#${NAMESPACE}#" ${TEMPLATE_DIR}/default.route.yaml > ${TARGET_DIR}/default.route.yaml

echo "IAM deployment for ${FQDN} generated in: ${TARGET_DIR}"
