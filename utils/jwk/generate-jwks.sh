#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

java -jar ${DIR}/jwk-gen.jar -t RSA -s 2048 -S -i rsa1 | tail -n +2 
