#!/bin/bash

kubectl get po --all-namespaces --selector=app=iam,tier=login-service -o jsonpath='{range .items[*]}{"\n"}{.metadata.namespace}{"/"}{.metadata.name}{":\t"}{range .spec.containers[*]}{.image}{", "}{end}{end}'
